﻿using System.Web;
using EPiServer.Personalization;
using FotoWareNet.FotoWeb;

namespace Fotoware.EPiServer.ImageProperty
{
    public class FotoWebTokenHandler : IHttpHandler
    {
        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            if (EpiserverSettings.Instance.SecretKey == null)
                return;

            var generator = new LoginTokenGenerator(EpiserverSettings.Instance.SecretKey);
            generator.UseForWidgets = true;

            string fotowebUser = EPiServerProfile.Current[Constants.ProfileFieldName] as string;
            if (string.IsNullOrEmpty(fotowebUser))
                return;

            var loginToken = generator.CreateLoginToken(fotowebUser);

            context.Response.Write(loginToken);
        }
    }
}
