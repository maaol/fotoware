﻿using System;
using System.ComponentModel;

namespace Fotoware.EPiServer.ImageProperty
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class FotowareConfigurationAttribute : Attribute
    {
        [Description("Default image height when retriving from fotoware.")]
        public int Height { get; set; }

        [Description("Default image width when retriving from fotoware.")]
        public int Width { get; set; }

        [Description("Default preset for fotoware image manipulation")]
        public string CustomPreset { get; set; }

        [Description(
            "Set true if image should  be stored on local media storage  (Double size image will be stored). Local url will be used to display image."
            )]
        public bool StoreFile { get; set; }
    }
}