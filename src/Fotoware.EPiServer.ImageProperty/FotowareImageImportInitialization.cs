﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using EPiServer;
using EPiServer.Core;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.ServiceLocation;

namespace Fotoware.EPiServer.ImageProperty
{
    /// <summary>
    /// Initialize store saveing to handle image upload on page save
    /// </summary>
    [ModuleDependency(typeof (ServiceContainerInitialization))]
    [InitializableModule]
    public class FotowareImageImportInitialization : IConfigurableModule
    {
        /// <summary>
        /// Register the default Image storage in default episerver IOC
        /// </summary>
        /// <param name="context">The context.</param>
        public void ConfigureContainer(ServiceConfigurationContext context)
        {
            context.Container.Configure(exp => exp.For<IImageStorage>().HttpContextScoped().Use<LocalPageImageStorage>());
        }

        public void Initialize(InitializationEngine context)
        {
            IContentEvents events = ServiceLocator.Current.GetInstance<IContentEvents>();
            events.SavingContent += OnSavingPage;
        }

        public void Uninitialize(InitializationEngine context)
        {
            IContentEvents events = ServiceLocator.Current.GetInstance<IContentEvents>();
            events.SavingContent -= OnSavingPage;
        }

        public void Preload(string[] parameters)
        {
        }

        private void OnSavingPage(object sender, ContentEventArgs e)
        {
            if (e.Content == null)
            {
                return;
            }

            CheckPageForImageToUpload(e.Content);
        }

        /// <summary>
        /// Find fotoware property on page data and handle the upload
        /// </summary>
        /// <param name="content">The page.</param>
        private void CheckPageForImageToUpload(IContent content)
        {
            foreach (PropertyData property in RetriveFotowareProperties(content))
            {
                if (!property.IsModified)
                    continue;

                FotowareConfigurationAttribute attributeConfiguration = GetPropertyConfigurationThroughReflection(content,
                    property);

                if (attributeConfiguration == null || !attributeConfiguration.StoreFile)
                    continue;

                HandleImageUpload(content, property);
            }
        }

        /// <summary>
        /// Handles the image upload.
        /// </summary>
        /// <param name="content">The page.</param>
        /// <param name="property">The property.</param>
        private void HandleImageUpload(IContent content, PropertyData property)
        {
            var fotowareImage = (FotowareImage) property.Value;
            if (fotowareImage == null)
                return;

            ContentReference newContentLink = UploadImage(fotowareImage.DoubleSizeUrl, content);
            if (ContentReference.IsNullOrEmpty(newContentLink))
                return;

            UpdateProperty(content, property, fotowareImage, newContentLink);
        }

        /// <summary>
        /// Uploads the image into the storage
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <param name="content">The content.</param>
        /// <returns></returns>
        private ContentReference UploadImage(string url, IContent content)
        {
            var imageStorage = ServiceLocator.Current.GetInstance<IImageStorage>();
            return imageStorage.SaveToStorage(url, content.ContentLink, string.Empty);
        }

        /// <summary>
        /// Updates the property
        /// </summary>
        /// <param name="content">The page.</param>
        /// <param name="property">The property.</param>
        /// <param name="fotowareImage">The fotoware image.</param>
        /// <param name="contentLink">The content link.</param>
        private void UpdateProperty(IContent content, PropertyData property, FotowareImage fotowareImage, ContentReference contentLink)
        {
            fotowareImage.ContentLink = contentLink;
            (content as ContentData).SetValue(property.Name, fotowareImage);
        }


        /// <summary>
        /// Retrives the fotoware properties from page data.
        /// </summary>
        /// <param name="pageData">The page data.</param>
        /// <returns></returns>
        private IEnumerable<PropertyData> RetriveFotowareProperties(IContent content)
        {
            return content.Property.Where(p => p.GetType() == typeof(FotowarePropertyBackingType));
        }

        /// <summary>
        /// Gets the property configuration FotowareConfigurationAttribute through reflection.
        /// </summary>
        /// <param name="content">Page data</param>
        /// <param name="property">Property to get config from.</param>
        /// <returns></returns>
        private FotowareConfigurationAttribute GetPropertyConfigurationThroughReflection(IContent content, PropertyData property)
        {
            PropertyInfo proeprtyInfo =
                content.GetOriginalType().GetProperties().FirstOrDefault(p => p.Name == property.Name);

            if (proeprtyInfo == null)
                return null;

            return proeprtyInfo.GetCustomAttributes(true).OfType<FotowareConfigurationAttribute>().FirstOrDefault();
        }
    }
}